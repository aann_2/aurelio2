using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovimientoEscalera : MonoBehaviour
{
    private float vertical;
    public float speed = 8f;
    private bool enEscalera;
    public bool enEscalada;
    public Rigidbody2D rb;
    private float _gravedad;
    public Animator animacion;
    public AudioSource ladder;

    private void Start() {
        _gravedad = rb.gravityScale;
    }
    void Update()
    {
        vertical = Input.GetAxis("Vertical"); 
        if( enEscalera && Mathf.Abs(vertical)>0f && !enEscalada) {
            enEscalada = true;
            rb.gravityScale = 0f;
            animacion.SetBool("Escaleras", true);
            
        }

        //if(enEscalada && rb.velocity.y != 0 && !ladder.isPlaying)
        //{
        //    ladder.Play();
        //}
    }


    private void FixedUpdate()
    {
        if (enEscalada) {
            rb.velocity = new Vector2(rb.velocity.x, vertical * speed);

            if (rb.velocity.y <= -0.3f || rb.velocity.y >= 0.3f)
            {
                animacion.speed = 1;
                if (!ladder.isPlaying)
                {
                    ladder.Play();
                }
            }
            else
            {
                animacion.speed = 0;
                if (ladder.isPlaying)
                {
                    ladder.Stop();
                }
            }
        }

        
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Escalera")) {
            enEscalera = true;
        }
        
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("Escalera")) {
            enEscalera = false;
            enEscalada = false;
            rb.gravityScale = _gravedad;
            animacion.SetBool("Escaleras", false);
            animacion.speed = 1;
            if (ladder.isPlaying)
            {
                ladder.Stop();
            }
        }

    }
}
