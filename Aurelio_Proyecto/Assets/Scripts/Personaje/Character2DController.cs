using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Character2DController : MonoBehaviour {
    //Variables movimiento basico
    public float MovementSpeed = 1;
    public float JumpForce = 1;
    private float JumpCount;
    private Rigidbody2D _rigidbody;
    public BoxCollider2D boxCollider2D;
    public float Caida;

    //Vida
    public static int health;
    public int maxHealth;

    //Box Animation
    public float distanciaRayBox;

    //Caida Animacion
    public Animator animacion;

    //Variables para Dash
    public float dashDistance = 15f;
    bool isDashing;
    float doubleTapTime;
    float mx;
    public float timeDashPositivo = 0.5f;
    public float timeDashNegativo = -0.5f;

    //Mascara coleccionables
    public LayerMask Coleccionables;

    //Variables de suelo
    public LayerMask ground;
    public float distanciaRay;

    //Colecionable movimiento
    public bool movimientoRapido;
    public ParticleSystem pVelocidad;

    //Coleccionable vida
    public GameObject pressE;

    //Coleccionable invencibilidad
    public bool invencible;
    public ParticleSystem pInvencible;

    //Coleccionable tiempo
    public float tiempo;

    //Checkpoint
    public Vector3 posicionIncial;

    //Aparici�n pociones
    public GameObject pocionInvencibilidad;
    public GameObject pocionDesplazamiento;

    //Variable Menu
    public GameObject endMenu;

    //Sistema de particulas
    public ParticleSystem velocidad;
    public ParticleSystem fuerza;

    //Sonido
    public AudioSource run;
    public AudioSource box;
    public AudioSource ambient;
    public AudioSource butter;
    public AudioSource potions;
    public AudioSource dash;

    public bool endFaded;

    public GameObject temporizador;

    void Start() {
        butter.Play();
        ambient.Play();
        _rigidbody = GetComponent<Rigidbody2D>();
        if((SceneManager.GetActiveScene().name == "Nivel1") && (health == 0)) {
            maxHealth = 3;
            health = maxHealth;
        }
        pressE.gameObject.SetActive(false);
        posicionIncial = transform.position;
        Cursor.visible = false;
    }

    // Update is called once per frame
    void Update() {

        pressE.gameObject.SetActive(false);
        var movement = Input.GetAxis("Horizontal");
        if (endFaded == true) {

        

        if (!isDashing) {
                //Movimiento
                
            if (movement <= -0.3f || movement >= 0.3f){
             mx = movement;
             temporizador.SetActive(true);
                   

            } else {
                mx = 0;
            }


            //Flip personaje
            if (Input.GetAxis("Horizontal") < 0) {
                GetComponent<SpriteRenderer>().flipX = false;
            }
            if (Input.GetAxis("Horizontal") > 0) {
                GetComponent<SpriteRenderer>().flipX = true;
            }

            //Animacion Correr
            if (movement <= -0.3f || movement >= 0.3f){
                animacion.SetBool("Correr", true);
                if (!run.isPlaying){
                    run.Play();
                }
                
                //Debug.Log("Iniciar sonido");
            } else {
                animacion.SetBool("Correr", false);
                run.Stop();
                //Debug.Log("Parar sonido");
            }


            //Animacion Caida / Salto
            if (_rigidbody.velocity.y < -0.01f) {
                //-0.01 para evitar conflicto entre caida y caja
                _rigidbody.AddForce(new Vector2(0, Caida), ForceMode2D.Impulse);
                animacion.SetBool("Caer", true);
                animacion.SetBool("Saltar", false);
                if (run.isPlaying)
                {
                    run.Stop();
                }
            } else {
                animacion.SetBool("Caer", false);
            }

            //Salto del personaje
            if (Input.GetButtonDown("Jump") && Mathf.Abs(_rigidbody.velocity.y) < 0.001f && IsGrounded()) {
                animacion.SetBool("Saltar", true);
                _rigidbody.AddForce(new Vector2(0, JumpForce), ForceMode2D.Impulse);
                JumpCount++;
                if (run.isPlaying)
                {
                    run.Stop();
                }       
            } else if (_rigidbody.velocity.y <= 0 && IsGrounded()) {
                JumpCount = 0;
            }

            //Dash 
            if (!GetComponent<MovimientoEscalera>().enEscalada && Input.GetKeyDown(KeyCode.LeftShift) && JumpCount <= 1) {
                if (!GetComponent<SpriteRenderer>().flipX) {
                    StartCoroutine(Dash(timeDashNegativo));
                    JumpCount++;
                } else {
                    StartCoroutine(Dash(timeDashPositivo));
                    JumpCount++;
                }
            }

        }

        RaycastHit2D hit2;
        if (GetComponent<SpriteRenderer>().flipX)
        {
            hit2 = Physics2D.Raycast(transform.position + Vector3.up, Vector2.right, distanciaRayBox, ground);
            //Debug.DrawRay(transform.position + Vector3.up + Vector3.right, Vector2.right, Color.red, 1);
            Debug.DrawLine(transform.position + Vector3.up, transform.position + Vector3.up + new Vector3(distanciaRayBox, 0, 0));
        }
        else
        {
            hit2 = Physics2D.Raycast(transform.position + Vector3.up, Vector2.left, distanciaRayBox, ground);
            //Debug.DrawRay(transform.position + Vector3.up + Vector3.left, Vector2.left, Color.red, 1);
            Debug.DrawLine(transform.position + Vector3.up, transform.position + Vector3.up + new Vector3(-distanciaRayBox, 0, 0));
        }

        if (hit2.collider != null && hit2.collider.CompareTag("Box") && (movement <= -0.3f || movement >= 0.3f))
        {
            //Debug.Log("funciona");
            animacion.SetBool("Empujar", true);
            if (!box.isPlaying)
            {
                box.Play();
            }
        } else
        {
            animacion.SetBool("Empujar", false);
            if (box.isPlaying)
            {
                box.Stop();
            }
        }


        //Recoger Coleccionable Vida
        RaycastHit2D hit;
        if (GetComponent<SpriteRenderer>().flipX) {
            hit = Physics2D.Raycast(transform.position + Vector3.up, Vector2.right, distanciaRay, Coleccionables);
            //Debug.DrawRay(transform.position + Vector3.up + Vector3.right, Vector2.right, Color.red, 1);
            Debug.DrawLine(transform.position + Vector3.up, transform.position + Vector3.up + new Vector3(distanciaRay, 0, 0));
        } else {
            hit = Physics2D.Raycast(transform.position + Vector3.up, Vector2.left, distanciaRay, Coleccionables);
            //Debug.DrawRay(transform.position + Vector3.up + Vector3.left, Vector2.left, Color.red, 1);
            Debug.DrawLine(transform.position + Vector3.up, transform.position + Vector3.up + new Vector3(- distanciaRay, 0, 0));
        }
            

        //Activar elemento pop up que muestre la tecla para coger el item y coja la vida
        if (hit.collider != null && hit.collider.CompareTag("Vida")) {
            pressE.gameObject.SetActive(true);
            if (Input.GetKeyDown(KeyCode.E)) {
                health++;
                Destroy(hit.collider.gameObject);
                pressE.gameObject.SetActive(false);
                potions.Play();
            }
            
        } 

        //Colecionable desplazamiento
        if (hit.collider != null && hit.collider.CompareTag("Movimiento")) {
            pressE.gameObject.SetActive(true);
            if (Input.GetKeyDown(KeyCode.E)) {
                movimientoRapido = true;
                MovementSpeed = 17;
                animacion.speed += 0.4f;
                Destroy(hit.collider.gameObject);
                Invoke("MovimientoRapidoFalse", 5.0f);
                pressE.gameObject.SetActive(false);
                pocionDesplazamiento.SetActive(true);
                potions.Play();
                var emission = velocidad.emission;
                emission.enabled = true;
            }
        }

        //Coleccionable invencibilidad
        if (hit.collider != null && hit.collider.CompareTag("Invencible")) {
            pressE.gameObject.SetActive(true);
            if (Input.GetKeyDown(KeyCode.E)) {
                invencible = true;
                Destroy(hit.collider.gameObject);
                Invoke("InvencibilidadFalse", 10f);
                pressE.gameObject.SetActive(false);
                pocionInvencibilidad.SetActive(true);
                potions.Play();
                var emission = fuerza.emission;
                emission.enabled = true;
            }
        }

        //Coleccionable tiempo
        if (hit.collider != null && hit.collider.CompareTag("Tiempo"))
        {
            pressE.gameObject.SetActive(true);
            if (Input.GetKeyDown(KeyCode.E))
            {
                GameObject.FindObjectOfType<TimeController>().timeValue += tiempo;
                Destroy(hit.collider.gameObject);
                pressE.gameObject.SetActive(false);
                potions.Play();
            }

        }
    }
    }

    // Consumible invencibilidad bool
    void InvencibilidadFalse()
    {
        invencible = false;
        pocionInvencibilidad.SetActive(false);
        var emission = fuerza.emission;
        emission.enabled = false;
    }

    // Consumible movimiento bool
    void MovimientoRapidoFalse() {
        movimientoRapido = false;
        MovementSpeed = 8;
        pocionDesplazamiento.SetActive(false);
        var emission = velocidad.emission;
        emission.enabled = false;
    }

    //Animacion de morir y despu�s volver a la posici�n inicial
    public void DIE() {
        if(health > 0){
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
            animacion.SetBool("Die", false);
        } else {
            Cursor.visible = true;
            Time.timeScale = 0;
            endMenu.SetActive(true);
        }
        
    }

    //Recibir da�o
    public void TakeHit() {
        if(invencible == false) {
            health--;
            animacion.SetBool("Die", true);
        }
    }



    //Dash
    void FixedUpdate() {
        if (!isDashing)
            _rigidbody.velocity = new Vector2(mx * MovementSpeed, _rigidbody.velocity.y);
    }
  
    //Dash
    IEnumerator Dash(float direction) {
        isDashing = true;
        animacion.SetBool("Dash", true);
        animacion.SetBool("Correr", false);
        _rigidbody.velocity = new Vector2(_rigidbody.velocity.x, 0f);
        _rigidbody.AddForce(new Vector2(dashDistance * direction, 0f), ForceMode2D.Impulse);
        float gravity = _rigidbody.gravityScale;
        _rigidbody.gravityScale = 0;
        yield return new WaitForSeconds(0.4f);
        isDashing = false;
        animacion.SetBool("Dash", false);
        _rigidbody.gravityScale = gravity;
        if (!dash.isPlaying)
        {
            dash.Play();
        }
    }

    //Mirar si toca el suelo
    bool IsGrounded() {
        Vector2 position = transform.position;
        Vector2 direction = Vector2.down;
        float distance = 1.2f;
        RaycastHit2D hit = Physics2D.Raycast(position, direction, distance, ground);
        //Debug.DrawRay(position, direction, Color.green, 1);
        Debug.DrawLine(position, position + new Vector2(0, -distanciaRay));

        if (hit.collider != null) {
            //Debug.Log(hit.collider.name);
            return true;
        }
        return false;
        
    }
   
}
