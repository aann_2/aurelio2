using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class seguimientocamara : MonoBehaviour
{
    public Vector2 minCamPos, mazCamPos;
    public GameObject seguir; 
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

        float posx = seguir.transform.position.x;
        float posy = seguir.transform.position.y;
        transform.position = new Vector3(Mathf.Clamp(posx, minCamPos.x, mazCamPos.x), Mathf.Clamp(posy, minCamPos.y, mazCamPos.y), transform.position.z);

    }
}
