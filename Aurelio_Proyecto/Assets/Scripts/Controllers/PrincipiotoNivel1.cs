using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using UnityEngine.SceneManagement;

public class PrincipiotoNivel1 : MonoBehaviour
{
    public VideoPlayer video;

    // Start is called before the first frame update
    void Start()
    {
        video.loopPointReached += EndReached;
    }


    void EndReached(UnityEngine.Video.VideoPlayer vp)
    {
        SceneManager.LoadScene("Nivel1");
    }
}
