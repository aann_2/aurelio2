using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Inicio : MonoBehaviour
{
    private void Start()
    {
        Time.timeScale = 1;
        Cursor.visible = true;
    }
    public void Play() {
        SceneManager.LoadScene("AnimacionPrincipio");
    }

    public void Exit() {
        Application.Quit();
    }
}
