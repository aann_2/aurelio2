using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Collecionable : MonoBehaviour
{
    public GameObject pared;
    public GameObject texto;
    public SpriteRenderer rendererEspada;
    public Collider2D colliderEspada;
    public AudioSource coleccionable;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player")) {
            colliderEspada.enabled = false;
            rendererEspada.enabled = false;
            Destroy(pared);
            texto.SetActive(true);
            if (!coleccionable.isPlaying)
            {
                coleccionable.Play();
            }
            Invoke("DesaparecerTexto", 3.0f);
        }
    }
    void DesaparecerTexto() {
        texto.SetActive(false);
        if (coleccionable.isPlaying)
        {
            coleccionable.Stop();
        }
        Destroy(gameObject);
    }
}
