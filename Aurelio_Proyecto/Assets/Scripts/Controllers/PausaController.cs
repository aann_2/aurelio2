using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PausaController : MonoBehaviour {

    //Pausa
    public bool pause;
    public GameObject pauseMenu;


    // Start is called before the first frame update
    void Start() {

        //Desactivar Pausa

        pauseMenu.SetActive(false);
        pause = false;
        Time.timeScale = 1;
    }

    // Update is called once per frame
    void Update() {

        //Activar Pausa
        
        if (Input.GetKeyDown(KeyCode.Escape)) {
            
            if (pause == true) {
                //Debug.Log("se ejecuta no pausa");
                pauseMenu.SetActive(false);
                pause = false;
                Time.timeScale = 1;
                Cursor.visible = false;
            }
            else {
                //Debug.Log("se ejecuta pausa");
                pause = true;
                Time.timeScale = 0.01f;
                pauseMenu.SetActive(true);
                Cursor.visible = true;
            }
        }
    }

    public void Resume() {
        pauseMenu.SetActive(false);
        Time.timeScale = 1;
        Cursor.visible = false;
    }

    public void QuitGame() {
        Application.Quit();
    }

    public void Inicio()
    {
        SceneManager.LoadScene("Inicio");
    }
}
