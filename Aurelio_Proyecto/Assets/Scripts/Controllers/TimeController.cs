using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using UnityEngine.SceneManagement;

public class TimeController : MonoBehaviour
{
    public float timeValue;
    float minutes;
    float seconds;
    public Text timeText;
    public GameObject endMenu;
    public bool endFaded;

    private void Start() {
        timeValue = 119;
        DisplayTime(timeValue);
        gameObject.SetActive(false);
    }

    private void Update() {
        //if(endFaded == true)
        //{

        
        if(timeValue > 0){
            timeValue -= Time.deltaTime;
        }
        else {
            timeValue = 0;
        }

        DisplayTime(timeValue);
        if(timeValue == 0){
            endMenu.SetActive(true);
            Cursor.visible = true;
            Time.timeScale = 0;
        }
        
    //}
    }

    void DisplayTime(float timeTodisplay) {
        if(timeTodisplay < 0) {
            timeTodisplay = 0;
        }
        else {
            timeTodisplay += 1;
        }
        minutes = Mathf.FloorToInt(timeTodisplay / 60);
        seconds = Mathf.FloorToInt(timeTodisplay % 60);
        timeText.text = string.Format("{0:00}:{1:00}", minutes, seconds);
    }
}
