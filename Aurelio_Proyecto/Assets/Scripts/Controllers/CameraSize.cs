using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraSize : MonoBehaviour
{
    public Animator camara;

    void Awake()
    {
        camara.SetBool("ZoomOut", false);
        camara.SetBool("ZoomIn", false);
    }

    private void OnTriggerEnter2D(Collider2D collision) {
        if (CompareTag("Cerca")) {
            camara.SetBool("ZoomOut", true);
        }
        if (CompareTag("Normal")) {
            camara.SetBool("ZoomOut", false);
        }
    }
}
