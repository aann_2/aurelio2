using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PasarAlNivel3 : MonoBehaviour
{
    public Animator fundido;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        SceneManager.LoadScene("Nivel3");
        fundido.SetBool("Transicion", true);
    }
}
