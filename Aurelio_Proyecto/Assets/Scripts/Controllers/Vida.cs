using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Vida : MonoBehaviour
{

    public GameObject full2;
    public GameObject full1;
    public GameObject vida4;
    public GameObject fullVida3;
    public GameObject full4;


    // Update is called once per frame
    void Update()
    {
        if (Character2DController.health == 4)
        {
            vida4.SetActive(true);
            full4.SetActive(true);
            fullVida3.SetActive(true);
            full2.SetActive(true);
            full1.SetActive(true);
        }

        if (Character2DController.health == 3) {
            full4.SetActive(false);
            fullVida3.SetActive(true);
            full2.SetActive(true);
            full1.SetActive(true);
        }

        if(Character2DController.health == 2) {
            full4.SetActive(false);
            fullVida3.SetActive(false);
            full2.SetActive(true);
            full1.SetActive(true);
        }

        if (Character2DController.health == 1)
        {
            full4.SetActive(false);
            fullVida3.SetActive(false);
            full2.SetActive(false);
            full1.SetActive(true);
        }

        if (Character2DController.health == 0)
        {
            full4.SetActive(false);
            fullVida3.SetActive(false);
            full2.SetActive(false);
            full1.SetActive(false);
        }
    }
}
