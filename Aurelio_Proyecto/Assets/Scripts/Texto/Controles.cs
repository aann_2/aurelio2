using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controles : MonoBehaviour
{
    public GameObject texto;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        texto.SetActive(true);
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        texto.SetActive(false);
    }
}
